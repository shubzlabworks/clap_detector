################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../tm4c123gh6pm.cmd 

C_SRCS += \
../main.c \
../timers.c \
../tm4c123gh6pm_startup_ccs.c \
../wait.c 

C_DEPS += \
./main.d \
./timers.d \
./tm4c123gh6pm_startup_ccs.d \
./wait.d 

OBJS += \
./main.obj \
./timers.obj \
./tm4c123gh6pm_startup_ccs.obj \
./wait.obj 

OBJS__QUOTED += \
"main.obj" \
"timers.obj" \
"tm4c123gh6pm_startup_ccs.obj" \
"wait.obj" 

C_DEPS__QUOTED += \
"main.d" \
"timers.d" \
"tm4c123gh6pm_startup_ccs.d" \
"wait.d" 

C_SRCS__QUOTED += \
"../main.c" \
"../timers.c" \
"../tm4c123gh6pm_startup_ccs.c" \
"../wait.c" 


