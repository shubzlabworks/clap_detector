/*
 * @brief Sequential-Clap based toggle
 *
 * @author Shubham Shrivastava
 */
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include "tm4c123gh6pm.h"
#include "wait.h"
#include "timers.h"

#ifndef TRUE
#define TRUE    1
#endif
#ifndef FALSE
#define FALSE   0
#endif

/* LED */
#define RED_LED             (*((volatile uint32_t *)(0x42000000 + (0x400253FC-0x40000000)*32 + 1*4)))
#define BLUE_LED            (*((volatile uint32_t *)(0x42000000 + (0x400253FC-0x40000000)*32 + 2*4)))
#define GREEN_LED           (*((volatile uint32_t *)(0x42000000 + (0x400253FC-0x40000000)*32 + 3*4)))
/* PushButton: PB3 */
#define PUSH_BUTTON         (*((volatile uint32_t *)(0x42000000 + (0x400253FC-0x40000000)*32 + 4*4)))
/* Sound detector input Pin: PC4 */
#define SOUND_DETECTOR_PIN  (*((volatile uint32_t *)(0x42000000 + (0x400063FC-0x40000000)*32 + 4*4)))
/* Output Pin: PC5 */
#define OUT_PIN             (*((volatile uint32_t *)(0x42000000 + (0x400063FC-0x40000000)*32 + 5*4)))

/**< Calib Parameters */
#define N_CLAPS_REQUIRED        2
#define MIN_TICKS_BETWEEN_CLAPS 200    // 1 tick = 1ms
#define MAX_TICKS_BETWEEN_CLAPS 1000

/* State enumeration */
enum eState {
    OFF=0,
    ON
};
/* Toggle State struct */
struct tToggleState {
    enum eState currentState;   // Current eState
    int clapCount;             // Number of claps detected since the reset eState
    uint32_t nTicksSinceLastClap;
} toggleState;

/* Init State function */
void initState() {
    toggleState.currentState = OFF;
    toggleState.clapCount = 0;
    toggleState.nTicksSinceLastClap = 0;
}
/* Reset State function */
void resetState() {
    toggleState.clapCount = 0;
    toggleState.nTicksSinceLastClap = 0;
}
/* Toggle current state */
void toggleCurrentState() {
    if(toggleState.currentState == OFF)
        toggleState.currentState = ON;
    else
        toggleState.currentState = OFF;
}
/* Check if the push-button was pressed */
bool isPbPressed() {
    /*
     * Push-Button is a default pull-up;
     * pushing it pulls the voltage level down
     */
    if(!PUSH_BUTTON) return TRUE;
    else return FALSE;
}
/* push-button detected boolean flag */
bool pbDetected    = FALSE;
/* clap detected boolean flag */
bool clapDetected  = FALSE;
/*
 * Blocking function that returns only when either SW1 is pressed,
 * or 'N_CLAPS_REQUIRED' claps are detected
 */
void waitTrigger() {
    while(1) {
        pbDetected     = isPbPressed();
        clapDetected   = (bool)SOUND_DETECTOR_PIN;
        if(pbDetected) {
            /* Toggle state */
            toggleCurrentState();
            break;
        }
        else if(clapDetected) {
            /* Wait for the detector output to go low */
            while(SOUND_DETECTOR_PIN);
            /* Increment clap count */
            toggleState.clapCount++;
            /* Check if the state should be toggled */
            if(toggleState.clapCount == N_CLAPS_REQUIRED) {
                if((toggleState.nTicksSinceLastClap >= MIN_TICKS_BETWEEN_CLAPS)
                   && (toggleState.nTicksSinceLastClap <=  MAX_TICKS_BETWEEN_CLAPS)) {
                    /* Toggle state */
                    toggleCurrentState();
                    break;
                }
                /*
                 * Reset the state if two claps were received either in
                 * a duration less than 'MIN_TICKS_BETWEEN_CLAPS' or greater
                 * than 'MAX_TICKS_BETWEEN_CLAPS'
                 */
                else resetState();
            }
        }
    }
}

/* Initialize hardware */
void initHw() {
    // Configure HW to work with 16 MHz XTAL, PLL enabled, system clock of 40 MHz
    SYSCTL_RCC_R = SYSCTL_RCC_XTAL_16MHZ | SYSCTL_RCC_OSCSRC_MAIN | SYSCTL_RCC_USESYSDIV | (4 << SYSCTL_RCC_SYSDIV_S);

    // Set GPIO ports to use APB (not needed since default configuration -- for clarity)
    // Note UART on port A must use APB
    SYSCTL_GPIOHBCTL_R = 0;
    // Enable GPIO port A, B, C, F peripherals
    SYSCTL_RCGC2_R |= SYSCTL_RCGC2_GPIOA | SYSCTL_RCGC2_GPIOB | SYSCTL_RCGC2_GPIOC | SYSCTL_RCGC2_GPIOF;


    // Configure UART0 pins
    SYSCTL_RCGCUART_R |= SYSCTL_RCGCUART_R0;         // turn-on UART0, leave other uarts in same status
    GPIO_PORTA_DEN_R |= 3;                           // default, added for clarity
    GPIO_PORTA_AFSEL_R |= 3;                         // default, added for clarity
    GPIO_PORTA_PCTL_R = GPIO_PCTL_PA1_U0TX | GPIO_PCTL_PA0_U0RX;

    // Configure LED
    // Green
    GPIO_PORTF_DIR_R |= 0x08;  // make bit 3 an output
    GPIO_PORTF_DR2R_R |= 0x08; // set drive strength to 2mA (not needed since default configuration -- for clarity)
    GPIO_PORTF_DEN_R |= 0x08;  // enable LED
    // Blue
    GPIO_PORTF_DIR_R |= 0x04;  // make bit 1 an outputs
    GPIO_PORTF_DR2R_R |= 0x04; // set drive strength to 2mA (not needed since default configuration -- for clarity)
    GPIO_PORTF_DEN_R |= 0x04;  // enable LED
    // Red
    GPIO_PORTF_DIR_R |= 0x02;  // make bit 1 an outputs
    GPIO_PORTF_DR2R_R |= 0x02; // set drive strength to 2mA (not needed since default configuration -- for clarity)
    GPIO_PORTF_DEN_R |= 0x02;  // enable LED

    // Configure pushbutton pins
    GPIO_PORTF_DIR_R &= ~0x10;  // make bit 4 an input
    GPIO_PORTF_DEN_R |= 0x10;  // enable pushbuttons
    GPIO_PORTF_PUR_R |= 0x10;  // enable internal pull-up for push button

    //Configure sound sensor digital input pin
    GPIO_PORTC_DIR_R &= ~0x10;  // make bit 4 an input
    GPIO_PORTC_DEN_R |= 0x10;  // enable pushbuttons
    GPIO_PORTC_PDR_R |= 0x10;  // enable internal pull-up for push button

    // Configure output pin
    GPIO_PORTC_DIR_R |= 0x20;  // make bit 5 output
    GPIO_PORTC_DR2R_R |= 0x20; // set drive strength to 2mA (not needed since default configuration -- for clarity)
    GPIO_PORTC_DEN_R |= 0x20;  // enable output pin

    // Configure UART0 to 115200 baud, 8N1 format (must be 3 clocks from clock enable and config writes)
    UART0_CTL_R = 0;                                 // turn-off UART0 to allow safe programming
    UART0_CC_R = UART_CC_CS_SYSCLK;                  // use system clock (40 MHz)
    UART0_IBRD_R = 21;                               // r = 40 MHz / (Nx115.2kHz), set floor(r)=21, where N=16
    UART0_FBRD_R = 45;                               // round(fract(r)*64)=45
    UART0_LCRH_R = UART_LCRH_WLEN_8 | UART_LCRH_FEN; // configure for 8N1 w/ 16-level FIFO
    UART0_CTL_R = UART_CTL_TXE | UART_CTL_RXE | UART_CTL_UARTEN; // enable TX, RX, and module
}

/* Function to write a character through UART0 */
void putcUart0(char c) {
    while (UART0_FR_R & UART_FR_TXFF);
    UART0_DR_R = c;
}
/* Blocking function that writes a string when the UART buffer is not full */
void putsUart0(char* str) {
    int i;
    for (i = 0; i < strlen(str); i++)
      putcUart0(str[i]);
}
/* Blocking function that returns with serial data once the buffer is not empty */
char getcUart0() {
    while (UART0_FR_R & UART_FR_RXFE);
    return UART0_DR_R & 0xFF;
}
/* Timer 1 Interrupt Service Routine */
void Timer1Isr() {
    if(toggleState.clapCount > 0) {
        toggleState.nTicksSinceLastClap++;
        /* Check for timeout */
        if(toggleState.nTicksSinceLastClap >= MAX_TICKS_BETWEEN_CLAPS) {
            /* Reset state if timeout occured */
            resetState();
        }
    }
    /* clear interrupt flag */
    TIMER1_ICR_R = TIMER_ICR_TATOCINT;
}
/* Timer 1 Interrupt Service Routine */
void Timer2Isr() {
    /* Code goes here */

    /* clear interrupt flag */
    TIMER2_ICR_R = TIMER_ICR_TATOCINT;
}
/* Get timer count from microseconds */
uint32_t countFromUs(float us) {
#define CLOCK_FREQ  40000000.0  //40MHz
#define US2SEC(sec) ((sec)/1000000.0)
    return (uint32_t)(US2SEC(us)*CLOCK_FREQ);
}

/* Main function */
int main(void) {
    /* Initialize Hardware */
    initHw();
    /* Initialize Timer */
    uint32_t microsec_timer = 1000; // 1 tick = 1ms
    uint32_t timer_count = countFromUs((float)microsec_timer);
    configureTim1(timer_count);
    /*
    putsUart0("##################################\r\n");
    putsUart0("#         CLAP DETECTOR          #\r\n");
    putsUart0("##################################\r\n");
    putsUart0("\r\nInitialization done!\r\n");
    */
    RED_LED = 1;
    waitMicrosecond(500000);
    RED_LED = 0;
    OUT_PIN = 0;
    initState();

    while(1) {
        /* Wait for either 'N_CLAPS_REQUIRED' claps or the push-button */
        waitTrigger();
        if(toggleState.currentState == ON) {
            /* Set output pin HIGH */
            OUT_PIN = 1;
        }
        else if(toggleState.currentState == OFF) {
            /* Set output pin LOW */
            OUT_PIN = 0;
        }
        /* Reset state */
        resetState();
        /* Turn on blue led for 100ms as an acknowledgement */
        BLUE_LED = 1;
        waitMicrosecond(100000);
        BLUE_LED = 0;
    }
    /* Control never reaches here */
    return 0;
}
