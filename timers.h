/*
 * timers.h
 *
 *  Created on: May 3, 2015
 *      Author: Shubham
 */

#ifndef TIMERS_H_
#define TIMERS_H_


void configureTim1(uint32_t timerCount);
void configureTim2(uint32_t timerCount);


#endif /* TIMERS_H_ */
